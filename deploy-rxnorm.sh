#!/bin/bash
#wget https://download.nlm.nih.gov/rxnorm/terminology_download_script.zip
#unzip terminology_download_script.zip 
#rm -rf terminology_download_script.zip
#rm -rf *.bat

#Default directory should contain following files : 
#Dir : Medicines
#under Medicines :
#					uts-cookie.txt
#					curl-uts-download.sh
#					uts.nlm.nih.gov.crt

#Starting to copy rxnsat
#ERROR:  value too long for type character varying(4000)
#CONTEXT:  COPY rxnsat, line 4364960, column atv: "BROWN(light brown')|N|4096
#799056|||6810439|AUI|55111-661|||DM_SPL_ID|MTHSPL|282091|N|4096

rm -rf rxnorm-siddharth
echo "Cloning rxnorm"
git clone git@bitbucket.org:radixers/rxnorm-siddharth.git

cd rxnorm-siddharth;
bash curl-uts-download.sh
unzip RxNorm_full_current.zip
sleep 2
rm -rf RxNorm_full_current.zip

#Formating rrf/RRF files
mv ./rrf/* .  #Moving files from ./rrf/ directory to current directory that is Medicines.
for i in `ls *.RRF`; do
  cat $i | sed 's/|$//g' | sed 's/\\|/|/g' > ./rrf/$i
done
rm -rf *.RRF
cp -rp ./rrf ./rrf-copy
echo ""
echo "rrf-copy folder duplicated successfully."
echo ""
#Formating prescribe/rrf/RRF files
mv ./prescribe/rrf/* .  #Moving files from ./prescribe/rrf/ directory to current directory that is Medicines.
for i in `ls *.RRF`; do
  cat $i | sed 's/|$//g' | sed 's/\\|/|/g' > ./prescribe/rrf/$i
done
rm -rf *.RRF
cp -rp ./prescribe/rrf ./prescribe/rrf-copy
echo ""
echo "/prescribe/rrf-copy folder duplicated successfully."
echo ""
#while   [ psql -d agatest -c "COPY import._days_diff FROM '/var/lib/postgresql/_pt.txt' CSV HEADER quote E'\b' delimiter E'\t'"  2>/var/lib/postgresql/errors.txt ] && [ ! -s /var/lib/postgresql/errors.txt ]
#
#do
#		cat /var/lib/postgresql/errors.txt|grep "line">gg.txt   #Here grep the line number and delete that line number from _pt.txt file 
#		hhh=$(grep line gg.txt | awk '{for(i=1; i<=NF; i++) if($i~/line/) print $(i+1)}'|cut -d ':' -f1)
#		sed -i $hhh'd' /var/lib/postgresql/_pt.txt
#		

#while [ psql -d rxnorm -c "copy rxnatomarchive from '/var/lib/postgresql/rxnorm-siddharth/rrf-copy/RXNATOMARCHIVE.RRF' with header csv delimiter E'|';" 2>/var/lib/postgresql/rxnorm-siddharth/errors.txt ] && [ ! -s /var/lib/postgresql/rxnorm-siddharth/errors.txt ]
#
#do
#		cat /var/lib/postgresql/rxnorm-siddharth/errors.txt|grep "line">error_line.txt
#		delete_line_number=$(grep line error_line.txt | awk '{for(i=1; i<=NF; i++) if($i~/line/) print $(i+1)}'|cut -d ':' -f1)
#		sed -i $delete_line_number'd' /var/lib/postgresql/rxnorm-siddharth/rrf-copy/RXNATOMARCHIVE.RRF
#		
#copy rxnconso from '/var/lib/postgresql/rxnorm-siddharth/rrf-copy/RXNCONSO.RRF' with header csv delimiter E'|';
#copy rxncuichanges from '/var/lib/postgresql/rxnorm-siddharth/rrf-copy/RXNCUICHANGES.RRF' with header csv delimiter E'|';
#copy rxncui from '/var/lib/postgresql/rxnorm-siddharth/rrf-copy/RXNCUI.RRF' with header csv delimiter E'|';
#copy rxndoc from '/var/lib/postgresql/rxnorm-siddharth/rrf-copy/RXNDOC.RRF' with header csv delimiter E'|';
#copy rxnrel from '/var/lib/postgresql/rxnorm-siddharth/rrf-copy/RXNREL.RRF' with header csv delimiter E'|';
#copy rxnsab from '/var/lib/postgresql/rxnorm-siddharth/rrf-copy/RXNSAB.RRF' with header csv delimiter E'|';
#copy rxnsat from '/var/lib/postgresql/rxnorm-siddharth/rrf-copy/RXNSAT.RRF' with header csv delimiter E'|';
#copy rxnsty from '/var/lib/postgresql/rxnorm-siddharth/rrf-copy/RXNSTY.RRF' with header csv delimiter E'|';
#

ls -l rrf|awk '{print $9}'|sed 1'd' > rrfilesnames.txt
while read filename
do 

tblname=`echo ${filename,,}|cut -d '.' -f1`

	psql -d rxnorm -c "copy ${tblname} from '$PWD/rrf-copy/${filename}' with header csv delimiter E'|';" 2>${PWD}/errors.txt ] && [ ! -s ${PWD}/errors.txt ]

		if [ ! -s ${PWD}/errors.txt ]; then
				
				cat ${PWD}/errors.txt|grep "line">error_line.txt
				delete_line_number=$(grep line error_line.txt | awk '{for(i=1; i<=NF; i++) if($i~/line/) print $(i+1)}'|cut -d ':' -f1)
				sed -i $delete_line_number'd' ${PWD}/rrf-copy/${filename}
		
		fi
done < rrfilesnames.txt
