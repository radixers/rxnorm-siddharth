--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.9
-- Dumped by pg_dump version 9.6.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: rxnatomarchive; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rxnatomarchive (
    rxaui character varying(8) NOT NULL,
    aui character varying(10),
    str character varying(4000) NOT NULL,
    archive_timestamp character varying(280) NOT NULL,
    created_timestamp character varying(280) NOT NULL,
    updated_timestamp character varying(280) NOT NULL,
    code character varying(50),
    is_brand character varying(1),
    lat character varying(3),
    last_released character varying(30),
    saui character varying(50),
    vsab character varying(40),
    rxcui character varying(8),
    sab character varying(20),
    tty character varying(20),
    merged_to_rxcui character varying(8)
);


ALTER TABLE public.rxnatomarchive OWNER TO postgres;

--
-- Name: rxnconso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rxnconso (
    rxcui character varying(8) NOT NULL,
    lat character varying(3) DEFAULT 'eng'::character varying NOT NULL,
    ts character varying(1),
    lui character varying(8),
    stt character varying(3),
    sui character varying(8),
    ispref character varying(1),
    rxaui character varying(8) NOT NULL,
    saui character varying(50),
    scui character varying(50),
    sdui character varying(50),
    sab character varying(20) NOT NULL,
    tty character varying(20) NOT NULL,
    code character varying(50) NOT NULL,
    str character varying NOT NULL,
    srl character varying(10),
    suppress character varying(1),
    cvf character varying(50)
);


ALTER TABLE public.rxnconso OWNER TO postgres;

--
-- Name: rxncui; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rxncui (
    cui1 character varying(8),
    ver_start character varying(40),
    ver_end character varying(40),
    cardinality character varying(8),
    cui2 character varying(8)
);


ALTER TABLE public.rxncui OWNER TO postgres;

--
-- Name: rxncuichanges; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rxncuichanges (
    rxaui character varying(8),
    code character varying(50),
    sab character varying(20),
    tty character varying(20),
    str character varying(3000),
    old_rxcui character varying(8) NOT NULL,
    new_rxcui character varying(8) NOT NULL
);


ALTER TABLE public.rxncuichanges OWNER TO postgres;

--
-- Name: rxndoc; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rxndoc (
    dockey character varying(50) NOT NULL,
    value character varying(1000),
    type character varying(50) NOT NULL,
    expl character varying(1000)
);


ALTER TABLE public.rxndoc OWNER TO postgres;

--
-- Name: rxnrel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rxnrel (
    rxcui1 character varying(8),
    rxaui1 character varying(8),
    stype1 character varying(50),
    rel character varying(4),
    rxcui2 character varying(8),
    rxaui2 character varying(8),
    stype2 character varying(50),
    rela character varying(100),
    rui character varying(10),
    srui character varying(50),
    sab character varying(20) NOT NULL,
    sl character varying(1000),
    dir character varying(1),
    rg character varying(10),
    suppress character varying(1),
    cvf character varying(50)
);


ALTER TABLE public.rxnrel OWNER TO postgres;

--
-- Name: rxnsab; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rxnsab (
    vcui character varying(8),
    rcui character varying(8),
    vsab character varying(40),
    rsab character varying(20) NOT NULL,
    son character varying(3000),
    sf character varying(20),
    sver character varying(20),
    vstart character varying(10),
    vend character varying(10),
    imeta character varying(10),
    rmeta character varying(10),
    slc character varying(1000),
    scc character varying(1000),
    srl character varying(100),
    tfr character varying(100),
    cfr character varying(100),
    cxty character varying(50),
    ttyl character varying(300),
    atnl character varying(1000),
    lat character varying(3),
    cenc character varying(20),
    curver character varying(1),
    sabin character varying(1),
    ssn character varying(3000),
    scit character varying(4000)
);


ALTER TABLE public.rxnsab OWNER TO postgres;

--
-- Name: rxnsat; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rxnsat (
    rxcui character varying(8),
    lui character varying(8),
    sui character varying(8),
    rxaui character varying(9),
    stype character varying(50),
    code character varying(50),
    atui character varying(11),
    satui character varying(50),
    atn character varying(1000) NOT NULL,
    sab character varying(20) NOT NULL,
    atv character varying(4000),
    suppress character varying(1),
    cvf character varying(50)
);


ALTER TABLE public.rxnsat OWNER TO postgres;

--
-- Name: rxnsty; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rxnsty (
    rxcui character varying(8) NOT NULL,
    tui character varying(4),
    stn character varying(100),
    sty character varying(50),
    atui character varying(11),
    cvf character varying(50)
);


ALTER TABLE public.rxnsty OWNER TO postgres;

--
-- Data for Name: rxnatomarchive; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rxnatomarchive (rxaui, aui, str, archive_timestamp, created_timestamp, updated_timestamp, code, is_brand, lat, last_released, saui, vsab, rxcui, sab, tty, merged_to_rxcui) FROM stdin;
\.


--
-- Data for Name: rxnconso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rxnconso (rxcui, lat, ts, lui, stt, sui, ispref, rxaui, saui, scui, sdui, sab, tty, code, str, srl, suppress, cvf) FROM stdin;
\.


--
-- Data for Name: rxncui; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rxncui (cui1, ver_start, ver_end, cardinality, cui2) FROM stdin;
\.


--
-- Data for Name: rxncuichanges; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rxncuichanges (rxaui, code, sab, tty, str, old_rxcui, new_rxcui) FROM stdin;
\.


--
-- Data for Name: rxndoc; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rxndoc (dockey, value, type, expl) FROM stdin;
\.


--
-- Data for Name: rxnrel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rxnrel (rxcui1, rxaui1, stype1, rel, rxcui2, rxaui2, stype2, rela, rui, srui, sab, sl, dir, rg, suppress, cvf) FROM stdin;
\.


--
-- Data for Name: rxnsab; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rxnsab (vcui, rcui, vsab, rsab, son, sf, sver, vstart, vend, imeta, rmeta, slc, scc, srl, tfr, cfr, cxty, ttyl, atnl, lat, cenc, curver, sabin, ssn, scit) FROM stdin;
\.


--
-- Data for Name: rxnsat; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rxnsat (rxcui, lui, sui, rxaui, stype, code, atui, satui, atn, sab, atv, suppress, cvf) FROM stdin;
\.


--
-- Data for Name: rxnsty; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rxnsty (rxcui, tui, stn, sty, atui, cvf) FROM stdin;
\.


--
-- PostgreSQL database dump complete
--

